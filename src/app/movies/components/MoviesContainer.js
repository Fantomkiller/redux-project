import React, {useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {getAllMovies} from "../duck/operations";

const MoviesContainer = (props) => {
	const dispatch = useDispatch();
	useEffect(()=> {
		dispatch(getAllMovies([500,550]))
	},[]);
	const movies = useSelector(state=> state.movies)
	return <ul>
		{movies.list.map(movie => <li key={movie}>{movie}</li>)}
	</ul>
}

export  default MoviesContainer
