import types from './types'
import produce from 'immer'

const INITIAL_STATE = {
	listName:'Favourite',
	list: [
		'Alien','Matrix','Terminator'
	]
};



const movieReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case types.ADD_MOVIE:
			return produce(state, draftState => {
				draftState.list.push(action.item)
			});
		case types.RESET_MOVIES:
			return produce(state, draftState=> {
				draftState.list = []
			});
		default:
			return state
	}
}

export {
	movieReducer
}
