import actions from "./actions";

const fetchMovieById = async (ids) => {
	const apiKey = 'xxx';
	let multipleResponses = [];
	for await (const id of ids) {
		const promise = await fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${apiKey}`,
		{method: 'GET'}).then(resp => resp.json())
			multipleResponses.push(promise);
	};

	return await multipleResponses;
};



export const getAllMovies = (ids) =>
	async (dispatch) => {
		const movies = await fetchMovieById(ids);
		movies.forEach(movie => dispatch(actions.add(movie.original_title)))
	};
