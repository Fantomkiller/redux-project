import { combineReducers } from 'redux'
import {actorReducer} from './app/actors/duck/reducers'
import {movieReducer} from './app/movies/duck/reducers'

const rootReducer = combineReducers({
	actors: actorReducer,
	movies: movieReducer
});

export default rootReducer
